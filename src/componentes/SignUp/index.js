import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import * as ROLES from '../../constants/roles';

import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';

const SignUpPage = () => (
  <div className="contenido">
    <SignUpForm />

  </div>
);

const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  isAdmin:false,
  error: null,
};

  class SignUpFormBase extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };

  }

  onSubmit = event => {
    const { username, email, passwordOne, isAdmin } = this.state;
    const roles = {ADMIN : " "};

    if (isAdmin) {
      roles[ROLES.ADMIN] = ROLES.ADMIN;
    }
    this.props.firebase
      .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // Create a user in your Firebase realtime database
        return this.props.firebase
          .user(authUser.user.uid)
          .set({
            username,
            email,
            roles,
          })
      })
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.LANDING);
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };



  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      isAdmin,
      error,
    } = this.state;
      
    const isInvalid =
    passwordOne !== passwordTwo ||
    passwordOne === '' ||
    email === '' ||
    username === '';

      return (
                <form onSubmit={this.onSubmit}>
                  
      <div className="login-wrap">
      {error && <div className="alert alert-danger">
      {error.message}</div>}
	      <div className="login-html">
		    <div className="login-form">
			    <div className="sign-in-htm" style={{display:"contents"}}>
				    <div className="group">
					<label htmlFor="username" className="label">Nombre de usuario</label>
          <input id="username" 
            type="text" 
            className="input" 
            name="username"
            value={username}
            onChange={this.onChange}
            type="text"
            placeholder="Nombre Completo"/>
				</div>
        <div className="group">
					<label htmlFor="email" className="label">Correo Electrónico</label>
					<input id="email" type="text" className="input" name="email"
          value={email}
          onChange={this.onChange}
          type="text"
          placeholder="Correo Electrónico"/>
				</div>
				<div className="group">
					<label htmlFor="passwordOne" className="label">Contraseña</label>
					<input id="passwordOne" className="input" data-type="password" 
          name="passwordOne"
          value={passwordOne}
          onChange={this.onChange}
          type="password"
          placeholder="Contraseña"/>
				</div>

        <div className="group">
					<label htmlFor="passTwo" className="label">Repite contraseña</label>
					<input id="passTwo" className="input" data-type="password" 
          name="passwordTwo"
          value={passwordTwo}
          onChange={this.onChange}
          type="password"
          placeholder="Repite Contraseña"/>
				</div>
				
				<div className="group">
        <input type="submit" disabled={isInvalid} className="button" value="Registrate"/>
          </div>
				<div className="hr"></div>
				
			</div>
			
		</div>
	</div>
</div>
</form>
        
      );
  }
}

const SignUpLink = () => (
  <p>
    Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);

const SignUpForm = compose(
    withRouter,
    withFirebase,
  )(SignUpFormBase);
  
export default SignUpPage;

export { SignUpForm, SignUpLink };