import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';


const config = {
    apiKey: "AIzaSyBBVasLtmWQWCV4Kv7MhTmuQQ2mfHO7aXg",
    authDomain: "sitio-727.firebaseapp.com",
    databaseURL: "https://sitio-727.firebaseio.com",
    projectId: "sitio-727",
    storageBucket: "",
    messagingSenderId: "411668969219",
    appId: "1:411668969219:web:8cd36f7b7be19b8d"
  };

  class Firebase {
    constructor() {
      app.initializeApp(config);
      this.auth = app.auth();
      this.db = app.database();

    }

      // *** Auth API ***
      doCreateUserWithEmailAndPassword = (email, password) =>
      this.auth.createUserWithEmailAndPassword(email, password);

      doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => {
    this.auth.signOut();
    }
    
   

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password =>
    this.auth.currentUser.updatePassword(password);

// *** Merge Auth and DB User API *** //

onAuthUserListener = (next, fallback) =>
this.auth.onAuthStateChanged(authUser => {
  if (authUser) {
    this.user(authUser.uid)
      .once('value')
      .then(snapshot => {
        const dbUser = snapshot.val();

        // default empty roles
        if (!dbUser.roles) {
          dbUser.roles = {};
        }

        // merge auth and db user
        authUser = {
          uid: authUser.uid,
          email: authUser.email,
          ...dbUser,
        };

        next(authUser);
      });
  } else {
    fallback();
  }
});

    // *** User API ***

    user = uid => this.db.ref(`users/${uid}`);
    Roleuser = uid => this.db.ref(`users/${uid}/roles`);

    users = () => this.db.ref('users');

  // *** desayuno API ***

  desayuno = uid => this.db.ref(`desayunos/${uid}`);

  desayunos = () => this.db.ref('desayunos');
  

    // *** almuerzo API ***

    almuerzo = uid => this.db.ref(`almuerzos/${uid}`);

    almuerzos = () => this.db.ref('almuerzos');
    
  
    // *** sandwtich API ***

    sandwitch = uid => this.db.ref(`sandwitches/${uid}`);

    sandwitches = () => this.db.ref('sandwitches');

       // *** pedidos API ***

       pedido = uid => this.db.ref(`pedidos/${uid}`);
       pedidoUnico = (uid, idPro) => this.db.ref(`pedidos/${uid}/${idPro}`);
       pedidos = () => this.db.ref('pedidos');

       // *** productos API ***

       producto = (tipo, uid) => this.db.ref(`${tipo}/${uid}`);
       productos = (tipo) => this.db.ref(`${tipo}`);
    }
  export default Firebase;
  