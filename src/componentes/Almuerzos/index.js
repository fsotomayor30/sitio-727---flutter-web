import {Card, Button, Row } from 'react-bootstrap';
import React, { Component } from 'react';
import { withFirebase } from '../Firebase';
import almuerzoImage from '../../imagenes/almuerzo.png';
import Spinner from 'react-spinner-material';
import dinero from '../../imagenes/dinero.png';
import { AuthUserContext } from '../Session';
import * as ROUTES from '../../constants/routes';
import { Link } from 'react-router-dom' 
import carro from '../../imagenes/carro.png';


const almuerzo = () => (
  <div className="contenido almuerzos">
    <AuthUserContext.Consumer>
    {authUser =>
      authUser ? (
        <Almuerzos authUser={authUser}/>      
      ) : (
        <Almuerzos authUser= "null"/>      
      )
    }
    </AuthUserContext.Consumer>
  </div>
);

class AlmuerzosBase extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      almuerzos: [],
      tipo:"almuerzos",
      mensaje: null,
      mensajeAlerta: null

    };
  }

  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.almuerzos().on('value', snapshot => {
      const messageObject = snapshot.val();

      if (messageObject) {
        const messageList = Object.keys(messageObject).map(key => ({
          ...messageObject[key],
          uid: key,
        }));

        this.setState({almuerzos: messageList,
          loading: false });
      } else {
        this.setState({ almuerzos: null, loading: false });
      }
    });
  }

  componentWillUnmount() {
    this.props.firebase.almerzos().off();
  }

  agregarAlCarro  = event => {
    if(this.props.authUser === "null"){
      this.setState({
        mensajeAlerta: "Necesitas iniciar sesión"
      })
      
      setTimeout( () => {this.setState({ 
        mensajeAlerta:null
    })}, 2000); 
  }else{
    this.setState({idProducto: event.target.name,
      mensaje: "Almuerzo agregado con éxito" });
    
       setTimeout( () => {this.setState({ 
        mensaje:null
     })}, 2000); 
     

     this.props.firebase.producto(this.state.tipo,  event.target.name).on('value', test => {
      console.log(this.state.tipo+"/"+event.target.name)
      const carroObject = test.val();
      

       this.props.firebase.pedido(this.props.authUser.uid).push({
        nombre: carroObject.nombre,
        descripcion: carroObject.descripcion,
        precio: carroObject.precio,
        confirmada: false
      });
    }) 
    
  }

  }

  render() {
    const { almuerzos, loading, mensaje, mensajeAlerta } = this.state;
    return (
      <div> 
        {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p className="no-contenido"><img className="logo" src={almuerzoImage} alt="almuerzo"/>Estamos buscando los mejores almuerzos Gourmet para ti ... </p>
        </div>}

        {almuerzos ? (
          <>
          <Row>
          {almuerzos.map(message => (
            <Card key={message.uid}>
              <Card.Img variant="top" src={almuerzoImage} />
              <Card.Body>
                <Card.Title>{message.nombre}</Card.Title>
                <Card.Text className="descripcion">
                  {message.descripcion}
                </Card.Text>
                <Card.Text className="precio">
                <img src={dinero} alt="precio"/>$ {Number(message.precio).toLocaleString()}
                </Card.Text>
                <Button variant="primary" name={message.uid} onClick={this.agregarAlCarro}>Agregar a mi carro</Button>
              </Card.Body>
            </Card>
          ))}
          {mensaje && <div className="alert seleccion alert-success">
          <img  src={carro} alt="carro"/>{mensaje}</div>}
        {mensajeAlerta && <div className="alert seleccion alert-danger">
        {mensajeAlerta}<Link to={ROUTES.SIGN_IN}> Quieres iniciar sesión?</Link></div>}
        </Row>
        
        </>
        ) : (
          <div className="no-contenido">No hay ningún almuerzo disponible ...</div>
        )}

        
      </div>
    );
  }

  
}


const Almuerzos = withFirebase(AlmuerzosBase);

export default almuerzo;