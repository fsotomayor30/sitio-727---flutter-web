import React, { Component } from 'react';
import { AuthUserContext } from '../Session';
import {Navbar, Nav, NavDropdown, Badge} from 'react-bootstrap';
import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import desayuno from '../../imagenes/desayuno.png';
import almuerzo from '../../imagenes/almuerzo.png';
import sandwitch from '../../imagenes/sandwitch.png';
import carro from '../../imagenes/carro.png';
import home from '../../imagenes/home.png';
import login from '../../imagenes/login.png';
import admin from '../../imagenes/admin.png';
import mas from '../../imagenes/mas.png';
import eliminar from '../../imagenes/eliminar.png';
import food from '../../imagenes/food.png';

import { withFirebase } from '../Firebase';



import * as ROLES from '../../constants/roles';


const Navigation = () => (
  <AuthUserContext.Consumer>
    {authUser =>
      authUser ? (
        <NavigationAuth authUser={authUser} />
      ) : (
        <NavigationNonAuth />
      )
    }
  </AuthUserContext.Consumer>
);


const NavigationAuth = ({ authUser}) => (
  <Navbar bg="dark" expand="lg" className="header">
    <Navbar.Brand href="/">Sitio 727</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
          <Nav.Link href={ROUTES.LANDING}><img src={home}/>Inicio</Nav.Link>
          <Nav.Link href={ROUTES.DESAYUNOS}> <img src={desayuno}/>Desayunos Gourmet</Nav.Link>
          <Nav.Link href={ROUTES.ALMUERZOS}> <img src={almuerzo}/>Almuerzos Gourmet</Nav.Link>
          <Nav.Link href={ROUTES.SANDWITCH}> <img src={sandwitch}/>Sandwitch Gourmet</Nav.Link>
          {/* <Nav.Link href={ROUTES.ACCOUNT}><img src={micuenta}/>Mi Cuenta</Nav.Link> */}
          <Nav.Link href={ROUTES.COMPRAS}> <img src={carro}/>Mis productos 
            <NumeroPedidos usuario={authUser}/>
            </Nav.Link>

          {!!authUser.roles[ROLES.ADMIN] && (
            <NavDropdown title="Admin" id="nav-dropdown">
            
              <NavDropdown.Item href={ROUTES.INGRESODESAYUNO}><img src={mas}/>Agregar Menú</NavDropdown.Item>
              <NavDropdown.Item href={ROUTES.ELIMINARDESAYUNO}><img src={eliminar}/>Eliminar Menú</NavDropdown.Item>
              <NavDropdown.Item href={ROUTES.ADMIN}><img src={admin}/>Usuarios</NavDropdown.Item>
              <NavDropdown.Item href={ROUTES.COCINA}><img src={food}/>Cocina</NavDropdown.Item>

          </NavDropdown>
          )}
          

      </Nav>
      <Nav.Link href={ROUTES.ACCOUNT}className="userConnect"><img src={admin}/>{authUser.email}</Nav.Link>
      <SignOutButton />
    </Navbar.Collapse>
  </Navbar> 

  
);

const NavigationNonAuth = () => (
  <Navbar bg="dark" expand="lg" className="header">
  <Navbar.Brand href="/">Sitio 727</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href={ROUTES.LANDING}><img src={home}/>Inicio</Nav.Link>
      <Nav.Link href={ROUTES.DESAYUNOS}> <img src={desayuno}/>Desayunos Gourmet</Nav.Link>
        <Nav.Link href={ROUTES.ALMUERZOS}> <img src={almuerzo}/>Almuerzos Gourmet</Nav.Link>
        <Nav.Link href={ROUTES.SANDWITCH}> <img src={sandwitch}/>Sandwitch Gourmet</Nav.Link>
    </Nav>
    <Nav.Link className="sign-in" href={ROUTES.SIGN_IN}><img src={login}/></Nav.Link>
      </Navbar.Collapse>
</Navbar> 

  
);

class Pedidos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      numeroPedidos: "0",
      micarro: []
    };
  }
  componentDidMount() {
    this.props.firebase.pedido(this.props.usuario.uid).on('value', snapshot => {
      const messageObject = snapshot.val();

      if (messageObject) {
        const messageList = Object.keys(messageObject).map(key => ({
          ...messageObject[key],
          uid: key,
        }));

        this.setState({numeroPedidos: messageList.length, micarro: messageList });
      } else {
        this.setState({ numeroPedidos:"0" });
      }
    });
  }

  componentWillUnmount() {
    this.props.firebase.desayunos().off();
  }

  render() {
    const {micarro} = this.state;

    let filtro = this.state.micarro.filter(
      (micarro) => {
        return micarro.confirmada === false;
      }
    );

    return (
        <Badge variant="light">{filtro.length}</Badge> 
    );
  }
}


const NumeroPedidos = withFirebase(Pedidos);


export default Navigation;