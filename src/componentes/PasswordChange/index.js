import React, { Component } from 'react';
import {Nav } from 'react-bootstrap';
import admin from '../../imagenes/admin.png';
import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';

const INITIAL_STATE = {
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class PasswordChangeForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { passwordOne } = this.state;

    this.props.firebase
      .doPasswordUpdate(passwordOne)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { passwordOne, passwordTwo, error } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo || passwordOne === '';

    return (
      <div className="contenido">
      <Nav fill variant="tabs" className="tabs" defaultActiveKey={ROUTES.PASSWORD_CHANGE}>
                <Nav.Item>
                    <Nav.Link href="/account"><img src={admin}/>Olvidé la clave</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href={ROUTES.PASSWORD_CHANGE}><img src={admin}/>Cambiar clave</Nav.Link>
                </Nav.Item>
                
    
            </Nav> 
      <div className="account">

        <form onSubmit={this.onSubmit}>
        <div className="login-wrap">
      {error && <div className="alert alert-danger">
{error.message}</div>}
	      <div className="login-html">
		    <div className="login-form">
			    <div className="sign-in-htm" style={{display:"contents"}}>
				  <div className="group">
					<label htmlFor="password1" className="label">Nueva contraseña</label>
          <input id="password1" type="password" className="input" 
          name="passwordOne"
          value={passwordOne}
          onChange={this.onChange}
          type="password"
          placeholder="Nueva Contraseña"/>
				</div>

        <div className="group">
					<label htmlFor="password2" className="label">Confirmar nueva contraseña</label>
          <input id="password2" type="password" className="input" 
          name="passwordTwo"
          value={passwordTwo}
          onChange={this.onChange}
          type="password"
          placeholder="Confirmar nueva contraseña"/>
				</div>
				
				
				<div className="group">
					<input type="submit" disabled={isInvalid} className="button" value="Reinciar contraseña"/>
				</div>
				<div className="hr"></div>
				
			</div>
			
		</div>
	</div>
</div>
        </form>
      </div>
      </div>
    );
  }
}

export default withFirebase(PasswordChangeForm);