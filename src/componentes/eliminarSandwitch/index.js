import React, { Component } from 'react';
import { withAuthorization } from '../Session';
import {Nav, Table } from 'react-bootstrap';
import desayuno from '../../imagenes/desayuno.png';
import almuerzo from '../../imagenes/almuerzo.png';
import sandwitch from '../../imagenes/sandwitch.png';
import * as ROLES from '../../constants/roles';
import Spinner from 'react-spinner-material';
import admin from '../../imagenes/admin.png';
import mas from '../../imagenes/mas.png';
import * as ROUTES from '../../constants/routes';


class eliminarSandwitch extends React.Component {
  

  constructor(props) {
    super(props);
    this.state = { 
        loading: false,
        sandwitches: [],
     };
  }

  componentWillUnmount() {
    this.props.firebase.sandwitches().off();
  }

  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.sandwitches().on('value', snapshot => {
      const sandwitchObject = snapshot.val();

      if (sandwitchObject) {
        const sandwitchList = Object.keys(sandwitchObject).map(key => ({
          ...sandwitchObject[key],
          uid: key,
        }));

        this.setState({sandwitches: sandwitchList,
          loading: false });
      } else {
        this.setState({ sandwitches: null, loading: false });
      }

      
    });
  }

  eliminarSandwitch = event => {

    this.props.firebase.sandwitch(event.target.value).remove();

    this.setState({
    
    })


  
  event.preventDefault();
}



  render() {
    const { sandwitches, loading } = this.state;
    

    return (
      <div className="contenido">
          {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p><img className="logo" src={admin} alt="almuerzo"/>Estamos cargando los sandwitch . . . </p>
            </div>}
            <Nav fill variant="tabs" className="tabs" defaultActiveKey="/eliminar-sandwitch">
                <Nav.Item>
                    <Nav.Link href="/eliminar-desayuno"><img src={desayuno}/>Desayunos</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/eliminar-almuerzo"><img src={almuerzo}></img>Almuerzos</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/eliminar-sandwitch"><img src={sandwitch}/>Sandwitch</Nav.Link>
                </Nav.Item>
    
            </Nav> 

            {sandwitches ? (
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sandwitches.map(sandwitch => (
                        <tr key={sandwitch.uid}>
                            <td> {sandwitch.nombre}</td>
                            <td> {sandwitch.descripcion}</td>
                            <td> $ {sandwitch.precio}</td>
                            <td>
                            <button type="submit" value={sandwitch.uid} className="sign-out" onClick={this.eliminarSandwitch} >
                            Eliminar
                                </button> 
                            </td>
                            
                        </tr>
                        ))}
                    </tbody>
                </Table>
            ) : (
                <div className="no-contenido dark">
                    <p>No hay ningún sandwitch disponible ...</p>
                    <p className="agregar">Agregar un sandwitch <a href={ROUTES.INGRESOSANDWITCH} className="agregar" >
                        <img src={mas}/></a>
                    </p>
                </div>
            )}

            
        </div>
    );

  }
}

const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];
  
export default withAuthorization(condition)(eliminarSandwitch);