import React from 'react';
import '../../App.css';
import logout from '../../imagenes/logout.png';


import { withFirebase } from '../Firebase';

const SignOutButton = ({ firebase }) => (
  <button type="submit" className="sign-out" onClick={firebase.doSignOut}>
        <img src={logout} alt="logout"/>

  </button>
);

export default withFirebase(SignOutButton);