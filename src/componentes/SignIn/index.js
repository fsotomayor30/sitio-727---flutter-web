import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';

const SignInPage = () => (
  <div className="contenido">
    <SignInForm />
    {/* <PasswordForgetLink /> */}
    {/* <SignUpLink /> */}
  </div>
);

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class SignInFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email, password } = this.state;

    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.LANDING);
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, password, error } = this.state;

    const isInvalid = password === '' || email === '';

    return (
      
      <form onSubmit={this.onSubmit}>
        
      <div className="login-wrap">
      {error && <div className="alert alert-danger">
{error.message}</div>}
	      <div className="login-html">
		        <div className="login-form">
			    <div className="sign-in-htm" style={{display:"contents"}}>
				    <div className="group">
					<label htmlFor="email" className="label">Correo Electrónico</label>
					<input id="email" type="text" className="input" name="email"
          value={email}
          onChange={this.onChange}
          type="text"
          placeholder="Correo electrónico"/>
				</div>
				<div className="group">
					<label htmlFor="pass" className="label">Contraseña</label>
					<input id="pass" className="input" data-type="password" name="password"
          value={password}
          onChange={this.onChange}
          type="password"
          placeholder="Contraseña"/>
				</div>
				
				<div className="group">
            <input type="submit" disabled={isInvalid} className="button" value="Iniciar Sesión"/>
				</div>
				<div className="hr"></div>
				<div className="foot-lnk">
					<a href={ROUTES.PASSWORD_FORGET}>Olvidaste la Clave?</a>
          <p style={{color:"white"}}>
          No tienes una cuenta? <a href={ROUTES.SIGN_UP}>Registrate</a>
          </p>
				</div>
			</div>
			
		</div>
	</div>
</div>



</form>
      /*
      <form onSubmit={this.onSubmit}>
        <input
          name="email"
          value={email}
          onChange={this.onChange}
          type="text"
          placeholder="Email Address"
        />
        <input
          name="password"
          value={password}
          onChange={this.onChange}
          type="password"
          placeholder="Password"
        />
        <button disabled={isInvalid} type="submit">
          Sign In
        </button>

      </form>*/
    );
  }
}

const SignInForm = compose(
  withRouter,
  withFirebase,
)(SignInFormBase);

export default SignInPage;

export { SignInForm };