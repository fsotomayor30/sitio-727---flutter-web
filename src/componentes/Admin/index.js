import React, { Component } from 'react';
import {Table, Nav} from 'react-bootstrap';
import { withAuthorization } from '../Session';
import { compose } from 'recompose';
import * as ROLES from '../../constants/roles';
import { withFirebase } from '../Firebase';
import admin from '../../imagenes/admin.png';
import Spinner from 'react-spinner-material';
import eliminar from '../../imagenes/eliminar.png';
import mas from '../../imagenes/mas.png';


class AdminPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      users: [],
    };
  }

  

  componentWillUnmount() {
    this.props.firebase.users().off();
  }

  
  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.users().on('value', snapshot => {
      const usersObject = snapshot.val();

      const usersList = Object.keys(usersObject).map(key => ({
        ...usersObject[key],
        uid: key,
      }));

      this.setState({
        users: usersList,
        loading: false,
      });
    });
  }

  darPrivilegios = event => {
      console.log(this.props.firebase.Roleuser(event.target.value));

      this.props.firebase.Roleuser(event.target.value).set({
        ADMIN: "ADMIN",
      });

      this.setState({
        mensaje: "Le has dado privilegios a un usuario" });
      
         setTimeout( () => {this.setState({ 
          mensaje:null
       })}, 2000); 
  

    
    event.preventDefault();
  }

  quitarPrivilegios = event => {

    this.props.firebase.Roleuser(event.target.value).set({
      ADMIN: "",
    });

    this.setState({
      mensaje2: "Le has quitado privilegios a un usuario" });
    
       setTimeout( () => {this.setState({ 
        mensaje2:null
     })}, 2000); 


  
  event.preventDefault();
}

  render() {
    const { users, loading, mensaje, mensaje2 } = this.state;
    

    return (
      <div className="contenido">
          {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p><img className="logo" src={admin} alt="almuerzo"/>Estamos cargando la lista  de Usuarios . . . </p>
        </div>}
        
        <Nav fill variant="tabs" className="tabs" defaultActiveKey="/admin">
          <Nav.Item>
            <Nav.Link href="/admin"><img src={admin}/>Usuarios</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link></Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link></Nav.Link>
          </Nav.Item>
        </Nav>
          
        <Table responsive striped bordered hover variant="dark">
  <thead>
    <tr>
      <th>ID</th>
      <th>Correo Electrónico</th>
      <th>Nombre Completo</th>
      <th>Administrador</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    {users.map(user => (
      <tr key={user.uid}>
          <td> {user.uid}</td>
          <td> {user.email}</td>
          <td> {user.username}</td>
          <td><input
            name="isAdmin"
            type="checkbox"
            readOnly
            checked={user.roles.ADMIN=="ADMIN" ? true : false}
          /></td>
          <td>
          {user.roles.ADMIN=="ADMIN" ? <button type="submit" value={user.uid} className="sign-out" onClick={this.quitarPrivilegios} >
          Quitar privilegios
  </button> : <button type="submit" value={user.uid} className="sign-in" onClick={this.darPrivilegios} >
          Dar privilegios
  </button>}
          </td>
          
      </tr>
    ))}
  </tbody>

</Table>
{mensaje && <div className="alert seleccion alert-success">
          <img  src={mas} alt="carro"/>{mensaje}</div>}
        {mensaje2 && <div className="alert seleccion alert-danger">
        <img  src={eliminar} alt="carro"/>{mensaje2}</div>}
      </div>
    );
  }
}






const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];

  export default compose(
    withAuthorization(condition),
    withFirebase,
  )(AdminPage);