import React, { Component } from 'react';
import { withAuthorization } from '../Session';
import {Nav, Table } from 'react-bootstrap';
import desayuno from '../../imagenes/desayuno.png';
import almuerzo from '../../imagenes/almuerzo.png';
import sandwitch from '../../imagenes/sandwitch.png';
import * as ROLES from '../../constants/roles';
import Spinner from 'react-spinner-material';
import admin from '../../imagenes/admin.png';
import * as ROUTES from '../../constants/routes';
import mas from '../../imagenes/mas.png';


class eliminarAlmuerzo extends React.Component {
  

  constructor(props) {
    super(props);
    this.state = { 
        loading: false,
        almuerzos: [],
     };
  }

  componentWillUnmount() {
    this.props.firebase.almuerzos().off();
  }

  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.almuerzos().on('value', snapshot => {
      const almuerzoObject = snapshot.val();

      if (almuerzoObject) {
        const almuerzoList = Object.keys(almuerzoObject).map(key => ({
          ...almuerzoObject[key],
          uid: key,
        }));
        this.setState(
        {almuerzos: almuerzoList,
          loading: false });
      } else {
        this.setState({ almuerzos: null, loading: false });
      }
    });
  }

  eliminarAlmuerzo = event => {

    this.props.firebase.almuerzo(event.target.value).remove();

    this.setState({
    
    })


  
  event.preventDefault();
}

  render() {
    const { almuerzos, loading } = this.state;
    

    return (
      <div className="contenido">
          {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p><img className="logo" src={admin} alt="almuerzo"/>Estamos cargando los almuerzos . . . </p>
            </div>}
        
            <Nav fill variant="tabs" className="tabs" defaultActiveKey="/eliminar-almuerzo">
                <Nav.Item>
                    <Nav.Link href="/eliminar-desayuno"><img src={desayuno}/>Desayunos</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/eliminar-almuerzo"><img src={almuerzo}></img>Almuerzos</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/eliminar-sandwitch"><img src={sandwitch}/>Sandwitch</Nav.Link>
                </Nav.Item>
    
            </Nav> 

            {almuerzos ? (
            <Table responsive striped bordered hover variant="dark">
                <thead>
                    <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {almuerzos.map(almuerzo => (
                    <tr key={almuerzo.uid}>
                        <td> {almuerzo.nombre}</td>
                        <td> {almuerzo.descripcion}</td>
                        <td> $ {almuerzo.precio}</td>
                        <td>
                        <button type="submit" value={almuerzo.uid} className="sign-out" onClick={this.eliminarAlmuerzo} >
                        Eliminar
                            </button> 
                        </td>
                        
                    </tr>
                    ))}
                </tbody>
                </Table>
        ) : (
          <div className="no-contenido dark">
            <p>No hay ningún almuerzos disponible ...</p>
            <p className="agregar">Agregar un almuerzo <a href={ROUTES.INGRESOALMUERZO} className="agregar" >
                        <img src={mas}/></a>
            </p></div>
          
        )}
            
        </div>
    );
  }
}

const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];
  
export default withAuthorization(condition)(eliminarAlmuerzo);