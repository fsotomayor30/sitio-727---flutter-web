import {Card, Button, Row } from 'react-bootstrap';
import React, { Component } from 'react';
import { withFirebase } from '../Firebase';
import carro from '../../imagenes/carro.png';
import desayunoImage from '../../imagenes/desayuno.png';
import Spinner from 'react-spinner-material';
import dinero from '../../imagenes/dinero.png';
import { AuthUserContext } from '../Session';
import { Link } from 'react-router-dom' 
import * as ROUTES from '../../constants/routes';

const desayuno = () => (
  <div className="contenido desayuno">
<AuthUserContext.Consumer>
{authUser =>
      authUser ? (
        <Desayunos authUser={authUser}/>      
      ) : (
        <Desayunos authUser= "null"/>      
      )
    }
    </AuthUserContext.Consumer>  </div>
);

class DesayunosBase extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      desayunos: [],
      tipo:"desayunos",
      mensaje: null,
      mensajeAlerta: null

    };
  }
  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.desayunos().on('value', snapshot => {
      const messageObject = snapshot.val();

      if (messageObject) {
        const messageList = Object.keys(messageObject).map(key => ({
          ...messageObject[key],
          uid: key,
        }));

        this.setState({desayunos: messageList,
          loading: false });
      } else {
        this.setState({ desayunos: null, loading: false });
      }
    });
  }

  componentWillUnmount() {
    this.props.firebase.desayunos().off();
  }

  agregarAlCarro  = event => {
    if(this.props.authUser === "null"){
      this.setState({
        mensajeAlerta: "Necesitas iniciar sesión"
      })
      
      setTimeout( () => {this.setState({ 
        mensajeAlerta:null
    })}, 2000); 

    }else{

    this.setState({idAlmuerzo: event.target.name,
      mensaje: "Desayuno agregado con éxito" });
    
       setTimeout( () => {this.setState({ 
        mensaje:null
     })}, 2000); 

     this.props.firebase.producto(this.state.tipo,  event.target.name).on('value', test => {
      const carroObject = test.val();
      

       this.props.firebase.pedido(this.props.authUser.uid).push({
        nombre: carroObject.nombre,
        descripcion: carroObject.descripcion,
        precio: carroObject.precio,
        confirmada: false

      });
    }) 
    
  }
  }

  render() {
    const { desayunos, loading, mensaje, mensajeAlerta } = this.state;
    return (
      <div>
        {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p><img className="logo" src={desayunoImage} alt="almuerzo"/>Estamos buscando los mejores desayunos Gourmet para ti . . . </p>
        </div>}

        {desayunos ? (
          <Row>
          {desayunos.map(message => (
            <Card key={message.uid}>
              <Card.Img variant="top" src={desayunoImage} />
              <Card.Body>
                <Card.Title>{message.nombre}</Card.Title>
                <Card.Text className="descripcion">
                  {message.descripcion}
                </Card.Text>
                <Card.Text className="precio">
                <img src={dinero} alt="precio"/>$ {Number(message.precio).toLocaleString()}
                </Card.Text>
                <Button variant="primary" name={message.uid} onClick={this.agregarAlCarro}>Agregar a mi carro</Button>
              </Card.Body>
            </Card>
          ))}
          {mensaje && <div className="alert seleccion alert-success">
              <img  src={carro} alt="carro"/>{mensaje}</div>}
              {mensajeAlerta && <div className="alert seleccion alert-danger">
        {mensajeAlerta}<Link to={ROUTES.SIGN_IN}> Quieres iniciar sesión?</Link></div>}
        </Row>
        ) : (
          <div className="no-contenido">No hay ningún almuerzo disponible ...</div>
        )}
      </div>
    );
  }

  
}




const Desayunos = withFirebase(DesayunosBase);

export default desayuno;