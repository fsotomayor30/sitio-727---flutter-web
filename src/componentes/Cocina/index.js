import {Card, Button, Row, Badge } from 'react-bootstrap';
import React, { Component } from 'react';
import { withFirebase } from '../Firebase';
import sandwitchImage from '../../imagenes/sandwitch.png';
import Spinner from 'react-spinner-material';
import dinero from '../../imagenes/dinero.png';
import { AuthUserContext, withAuthorization } from '../Session';
import carro from '../../imagenes/carro.png';
import check from '../../imagenes/comprobado.png';


const cocina = () => (
    <div className="contenido sandwitch">
    <AuthUserContext.Consumer>
    {authUser =>
        <Cocina authUser={authUser}/>      
      
    }
    </AuthUserContext.Consumer>  
  </div>
);

class CocinaBase extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      loading2: false,
      micarro: [],
      mensaje: null,
      mensajeAlerta: null,
      total: 0,

    };
  }

  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.pedido(this.props.authUser.uid).on('value', snapshot => {
      const messageObject = snapshot.val();
      let total=0;
      if (messageObject) {
         const messageList = Object.keys(messageObject).map(key => ({
            ...messageObject[key],
            uid: key,
          })); 

          messageList.map((pedido,key) => (
            (pedido.confirmada === true) ? (
              total=total
              ) : (
                total=(total+Number(pedido.precio))
              )
          ));

          this.setState({micarro: messageList,
          loading: false, total: total });

      } else {
        this.setState({ micarro: null, loading: false });

      }
    });


  }

 
  componentWillUnmount() {
    this.props.firebase.pedido().off();
  }

  eliminar  = event => {
    this.props.firebase.pedidoUnico(this.props.authUser.uid,event.target.name).remove();
    console.log(this.props.authUser.uid+"/"+event.target.name)

    this.setState({
      mensaje: "Pedido eliminado con éxito" });
    
       setTimeout( () => {this.setState({ 
        mensaje:null
     })}, 2000); 

    event.preventDefault();
  }

  confirmar  = () => {
    this.state.micarro.map((key, carro) => (
      this.props.firebase.pedidoUnico(this.props.authUser.uid,key.uid).set({
        confirmada: true,
        nombre: key.nombre,
        descripcion: key.descripcion,
        precio: key.precio,
        uid: key.uid
      })
    ));
  }

  render() {
    const {loading, micarro, mensaje, total } = this.state;
    let filtro = this.state.micarro.filter(
      (micarro) => {
        return micarro.confirmada === true;
      }
    );
 


    return (
      <div>
        {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p><img className="logo" src={carro} alt="almuerzo"/>Estamos revisando tu carro de compras . . . </p>
        </div>}
        { micarro ? (
          <>
          <Row>
        

          {filtro.map((message, i) => {
              return (
                  
                <Card key={message.uid}>
                <Card.Body>
                  <Card.Title>{message.nombre}</Card.Title>
                  <Card.Text className="descripcion">
                    {message.descripcion}
                  </Card.Text>
                  <Card.Text className="precio">
                  <img src={dinero} alt="precio"/>$ {Number(message.precio).toLocaleString()}
                  </Card.Text>
                  <Button variant="primary" name={message.uid} onClick={this.eliminar}>Eliminar</Button>
                </Card.Body>
              </Card>
              )
            })}

          
          
          {mensaje  && <div className="alert seleccion alert-danger">
          <img  src={carro} alt="carro"/>{mensaje}</div>}
        </Row>


        </>
          ) : (
          <div className="no-contenido">Tu carro de compras esta vacio ...</div>
        )}
      </div>
    );
  }

  
}

const Cocina = withFirebase(CocinaBase);


const condition = authUser => !!authUser;
export default withAuthorization(condition)(cocina);


