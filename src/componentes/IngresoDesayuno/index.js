import React, { Component } from 'react';
import { withAuthorization } from '../Session';
import {Form, Button, InputGroup, Nav } from 'react-bootstrap';
import desayuno from '../../imagenes/desayuno.png';
import almuerzo from '../../imagenes/almuerzo.png';
import sandwitch from '../../imagenes/sandwitch.png';
import * as ROLES from '../../constants/roles';
import menos from '../../imagenes/eliminar.png';
import mas from '../../imagenes/mas.png';
import * as ROUTES from '../../constants/routes';

class IngresoDesayuno extends React.Component {
  

  constructor(props) {
    super(props);
    this.state = { 
      nombredesayuno: '',
      descripciondesayuno: '',
      preciodesayuno: '',
      validated: false,
      mensaje: null,
      mensaje2: null

     };
  }

  onCreateMessage = event => {

    if(!(this.state.nombredesayuno === '' && this.state.nombredesayuno === '' && this.state.preciodesayuno ==='')){
      this.props.firebase.desayunos().push({
        nombre: this.state.nombredesayuno,
        descripcion: this.state.descripciondesayuno,
        precio: this.state.preciodesayuno
      });

      this.setState({
        mensaje: "Desayuno agregado con éxito"
      })

      setTimeout( () => {this.setState({ 
        mensaje:null
     })}, 2000);
  }else{
    this.setState({
      mensaje2: "Tienes que completar todos los campos"
    })
    
    setTimeout( () => {this.setState({ 
      mensaje2:null
   })}, 2000);
  }

    event.preventDefault();
  };

  
  onChangeText = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { nombredesayuno, descripciondesayuno, preciodesayuno, mensaje, mensaje2 } = this.state;

    return(
  <div className="contenido">
    <Nav fill variant="tabs" className="tabs" defaultActiveKey="/ingreso-desayuno">
  <Nav.Item>
    <Nav.Link href="/ingreso-desayuno"><img src={desayuno}/>Desayunos</Nav.Link>
  </Nav.Item>
  <Nav.Item>
    <Nav.Link href="/ingreso-almuerzo"><img src={almuerzo}></img>Almuerzos</Nav.Link>
  </Nav.Item>
  <Nav.Item>
    <Nav.Link href="/ingreso-sandwitch"><img src={sandwitch}/>Sandwitch</Nav.Link>
  </Nav.Item>
  
</Nav>
        <Form
        className="form-ingreso"
        onSubmit={this.onCreateMessage}>  

      <Form.Group controlId="formBasicNombre">
        <Form.Label>Nombre</Form.Label>
        <Form.Control 
          name="nombredesayuno"
          required 
          type="text" 
          value={nombredesayuno}
          placeholder="Nombre desayuno"
          onChange={this.onChangeText}/>
          
      </Form.Group>

      <Form.Group controlId="formBasicDescripcion">
        <Form.Label>Descripción</Form.Label>
        <Form.Control 
          required
          name="descripciondesayuno"
          type="text" 
          value={descripciondesayuno}
          placeholder="Descripcion desayuno"
          onChange={this.onChangeText} />
          
      </Form.Group>

      <Form.Group controlId="formBasicPrecio">
        <Form.Label>Precio</Form.Label>
        <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text id="inputGroupPrepend">$</InputGroup.Text>
              </InputGroup.Prepend>
        <Form.Control
          aria-describedby="inputGroupPrepend" 
          type="number" 
          placeholder="Precio desayuno"
          required
          value={preciodesayuno}
          name="preciodesayuno"
          onChange={this.onChangeText}
          min="0" />
          
          </InputGroup>

      </Form.Group>
      <div className="buttons">
              <Button variant="primary" type="submit">
                <img src={mas}/>Agregar Desayuno
              </Button>
              <a href={ROUTES.ELIMINARDESAYUNO} className="btn btn-primary" >
                <img src={menos}/>Eliminar Desayuno
              </a>
            </div>
</Form>  
{mensaje && <div className="alert seleccion alert-success">
          <img  src={almuerzo} alt="carro"/>{mensaje}</div>}
        {mensaje2 && <div className="alert seleccion alert-danger">
        <img  src={almuerzo} alt="carro"/>{mensaje2}</div>}</div>
);
}
}

const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];
  
export default withAuthorization(condition)(IngresoDesayuno);