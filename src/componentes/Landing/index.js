import React from 'react';
import {Carousel, Card, Button} from 'react-bootstrap';
import desayuno from '../../imagenes/desayunogourmet1.jpg';
import almuerzo from '../../imagenes/almuerzogourmet1.jpg';
import sandwitch from '../../imagenes/sandwitch.jpg';
import mapa from '../../imagenes/MAPA.png';
import marker from '../../imagenes/marker.png';



const landing = () => (
  <div >
        <Carousel>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={desayuno}
              alt="desayuno"
            />
            <Carousel.Caption>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={almuerzo}
              alt="almuerzo"
            />

            <Carousel.Caption>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={sandwitch}
              alt="sandwitch"
            />

            <Carousel.Caption>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>  
  <div className="contenido">
    <h1 className="titulo">
      La mejor comida Gourmet en Sitio 727
    </h1>
    <Card className="ubicacion" >
  <Card.Img variant="top" src={mapa} />
  <Card.Body>
    <div  className="texto">
    <Card.Title  >Donde estámos ubicados?</Card.Title>
    <Card.Text>
     Estamos ubicados en Av. Grecia 8735, Peñalolén, SANTIAGO
    </Card.Text>
    </div>
    <Card.Img variant="top" src={marker} />
  </Card.Body>
</Card>
  </div>
  </div>
);

export default landing;