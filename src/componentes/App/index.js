import '../../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import {BrowserRouter as Router,Route} from 'react-router-dom';
import Navigation from '../Navigation';
import LandingPage from '../Landing';
import SignUpPage from '../SignUp';
import SignInPage from '../SignIn';
import PasswordForgetPage from '../PasswordForget';
import PasswordChange from '../PasswordChange';

import HomePage from '../IngresoDesayuno';
import AccountPage from '../Account';
import AdminPage from '../Admin';
import Desayunos from '../Desayunos';
import Almuerzos from '../Almuerzos';
import Sandwitch from '../Sandwitch';
import IngresoDesayuno from '../IngresoDesayuno';
import IngresoAlmuerzo from '../IngresoAlmuerzo';
import IngresoSandwitch from '../IngresoSandwitch';
import Carro from '../Pedidos';

import EliminarDesayuno from '../eliminarDesayuno';
import EliminarAlmuerzo from '../eliminarAlmuerzo';
import EliminarSandwitch from '../eliminarSandwitch';

import Cocina from '../Cocina';


import { withAuthentication, AuthUserContext } from '../Session';

import * as ROUTES from '../../constants/routes';



const App = () => (

  <Router>
    <div >
    
    <Navigation usuario={AuthUserContext}/>
      <Route exact path={ROUTES.LANDING} component={LandingPage} />
      <Route path={ROUTES.SIGN_UP} component={SignUpPage} />
      <Route path={ROUTES.SIGN_IN} component={SignInPage} />
      <Route path={ROUTES.PASSWORD_FORGET} component={PasswordForgetPage} />
      <Route path={ROUTES.HOME} component={HomePage} />
      <Route path={ROUTES.ACCOUNT} component={AccountPage} />
      <Route path={ROUTES.ADMIN} component={AdminPage} />
      <Route path={ROUTES.DESAYUNOS} component={Desayunos} />
      <Route path={ROUTES.ALMUERZOS} component={Almuerzos} />
      <Route path={ROUTES.SANDWITCH} component={Sandwitch} />
      <Route path={ROUTES.INGRESODESAYUNO} component={IngresoDesayuno} />
      <Route path={ROUTES.INGRESOALMUERZO} component={IngresoAlmuerzo} />
      <Route path={ROUTES.INGRESOSANDWITCH} component={IngresoSandwitch} />
      <Route path={ROUTES.ELIMINARDESAYUNO} component={EliminarDesayuno} />
      <Route path={ROUTES.ELIMINARALMUERZO} component={EliminarAlmuerzo} />
      <Route path={ROUTES.ELIMINARSANDWITCH} component={EliminarSandwitch} />
      <Route path={ROUTES.COMPRAS} component={Carro} />
      <Route path={ROUTES.PASSWORD_CHANGE} component={PasswordChange} />
      <Route path={ROUTES.COCINA} component={Cocina} />

    </div>
  </Router>
  );




export default withAuthentication(App);
