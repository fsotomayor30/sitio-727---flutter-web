import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';
import {Nav } from 'react-bootstrap';
import admin from '../../imagenes/admin.png';

const PasswordForgetPage = () => (
  <div>
    <PasswordForgetForm />
  </div>
);

const INITIAL_STATE = {
  email: '',
  error: null,
};

class PasswordForgetFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email } = this.state;

    this.props.firebase
      .doPasswordReset(email)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, error } = this.state;

    const isInvalid = email === '';

    return (
      <>
      <Nav fill variant="tabs" className="tabs" defaultActiveKey="/account">
                <Nav.Item>
                    <Nav.Link href="/account"><img src={admin}/>Olvidé la clave</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href={ROUTES.PASSWORD_CHANGE}><img src={admin}/>Cambiar clave</Nav.Link>
                </Nav.Item>
               
    
            </Nav> 
      <div className="account">
              <form onSubmit={this.onSubmit}>
      <div className="login-wrap">
      {error && <div className="alert alert-danger">
{error.message}</div>}
	      <div className="login-html">
		    <div className="login-form">
			    <div className="sign-in-htm" style={{display:"contents"}}>
				    <div className="group">
					<label htmlFor="user" className="label">Correo Electrónico</label>
					<input id="user" type="text" className="input" name="email"
          value={this.state.email}
          onChange={this.onChange}
          type="text"
          placeholder="Correo Electrónico"/>
				</div>
				
				
				<div className="group">
					<input type="submit" disabled={isInvalid} className="button" value="Recuperar contraseña"/>
				</div>
				<div className="hr"></div>
				
			</div>
			
		</div>
	</div>
</div>
</form>
      
      </div>
      </>
    );
  }
}

const PasswordForgetLink = () => (
  <p>
    <Link to={ROUTES.PASSWORD_FORGET}>Forgot Password?</Link>
  </p>
);

export default PasswordForgetPage;

const PasswordForgetForm = withFirebase(PasswordForgetFormBase);

export { PasswordForgetForm, PasswordForgetLink };